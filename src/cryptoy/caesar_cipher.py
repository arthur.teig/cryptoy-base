from cryptoy.utils import (
    str_to_unicodes,
    unicodes_to_str,
)

# TP: Chiffrement de César


def encrypt(msg: str, shift: int) -> str:
    res = str_to_unicodes(msg)
    for i in range(len(res)):
        res[i] = res[i] + shift
    return res


def decrypt(msg: str, shift: int) -> str:
    for i in range(len(msg)):
        msg[i] = msg[i] - shift
    res = unicodes_to_str(msg)
    return res

def attack() -> tuple[str, int]:
    s = "恱恪恸急恪恳恳恪恲恮恸急恦恹恹恦恶恺恪恷恴恳恸急恵恦恷急恱恪急恳恴恷恩怱急恲恮恳恪恿急恱恦急恿恴恳恪"
    for i in range(1, 0x110000):
        
        res = decrypt(str_to_unicodes(s), i)
        if (res.__contains__("ennemis")):
            return (res, i)
    raise RuntimeError("Failed to attack")
